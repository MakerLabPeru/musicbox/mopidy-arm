# libspotify only supports 32 bit arm
FROM arm32v7/debian:stretch-slim AS base

# Install updates
RUN apt-get update
RUN apt-get dist-upgrade -y
RUN apt-get install -y curl gnupg

# Add mopidy repository
RUN curl -L https://apt.mopidy.com/mopidy.gpg | apt-key add -
RUN curl -L https://apt.mopidy.com/mopidy.list -o /etc/apt/sources.list.d/mopidy.list
RUN apt-get update

# Install dependencies
RUN apt-get install -y gcc dumb-init gstreamer1.0-alsa gstreamer1.0-plugins-bad python-crypto

# Install Mopidy
RUN apt-get install -y mopidy mopidy-spotify

# Install pip
RUN curl -L https://bootstrap.pypa.io/get-pip.py | python -
# Install pip libraries
RUN pip install six cryptography pyasn1 pyopenssl requests[security] youtube-dl
RUN pip install \
    Mopidy-YouTube \
    Mopidy-API-Explorer \
    Mopidy-Iris \
    Mopidy-Mopify \
    Mopidy-MusicBox-Webclient \
    Mopidy-Party

# Clean-up
RUN apt-get purge --auto-remove -y curl gcc
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache


FROM base AS release

# Default configuration
COPY mopidy.conf /var/lib/mopidy/.config/mopidy/mopidy.conf
# Start helper script
COPY entrypoint.sh /entrypoint.sh

# Limited access rights.
RUN chown mopidy:audio -R /var/lib/mopidy/.config
RUN chown mopidy:audio /entrypoint.sh

# Run as mopidy user
USER mopidy

VOLUME ["/var/lib/mopidy/local", "/var/lib/mopidy/media"]

EXPOSE 6600 6680

ENTRYPOINT ["/entrypoint.sh"]
CMD ["mopidy"]
