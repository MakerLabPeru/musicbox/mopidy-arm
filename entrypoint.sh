#!/usr/bin/dumb-init /bin/bash

# if command starts with an option, prepend mopidy
if [ "${1:0:1}" = '-' ]; then
	  set -- mopidy "$@"
fi

: ${SECRETS_DIR:=/run/secrets}

# If wanted mopidy,  append default config
if [ "$1" = 'mopidy' ]; then
    if [ -f "${SECRETS_DIR}/spotify_username" ]; then
        set -- "$@" -o spotify/username=$(cat "${SECRETS_DIR}/spotify_username")
    fi

    if [ -f "${SECRETS_DIR}/spotify_password" ]; then
        set -- "$@" -o spotify/password=$(cat "${SECRETS_DIR}/spotify_password")
    fi

    if [ -f "${SECRETS_DIR}/spotify_client_id" ]; then
        set -- "$@" -o spotify/client_id=$(cat "${SECRETS_DIR}/spotify_client_id")
    fi

    if [ -f "${SECRETS_DIR}/spotify_client_secret" ]; then
        set -- "$@" -o spotify/client_secret=$(cat "${SECRETS_DIR}/spotify_client_secret")
    fi
fi
exec "$@"
